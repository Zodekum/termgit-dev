Instance: test-valueset 
InstanceOf: ValueSet 
Usage: #definition 
* id = "test-valueset" 
* url = "https://Zodekum.gitlab.com/ValueSet/test-valueset" 
* name = "test-valueset" 
* title = "Test eines ValueSets" 
* status = #draft 
* version = "1.0.1+20230314" 
* description = "**TEST**ValueSet" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.999.1.999.1" 
* date = "2023-03-14" 
* contact[0].name = "Mein Kontakt" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.bura.at/appc/" 
* contact[1].name = "Mein Zweiterkontakt" 
* contact[1].telecom[0].system = #url 
* contact[1].telecom[0].value = "http://www.bura.at/appc/" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/appc-anatomie"
* compose.include[0].version = "1.0.0+20230131"
* compose.include[0].concept[0].code = "0"
* compose.include[0].concept[0].display = "Maßnahmen"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "Übersetzte Maßnahme" 
* compose.include[0].concept[1].code = "1"
* compose.include[0].concept[1].display = "Maßnahmen1"
* compose.include[0].concept[1].designation[0].use = https://termgit.elga.at/CodeSystem/austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[1].designation[0].value = "Achtung nur für Kinder" 
* compose.include[0].concept[2].code = "2"
* compose.include[0].concept[2].display = "Maßnahmen2"
